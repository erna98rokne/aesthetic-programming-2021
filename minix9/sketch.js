let url = "https://api.giphy.com/v1/gifs/search?api_key=fzPJXS83lvCORZBSv9AUoGYX3WiC3gS3";
/*our url consists of the search url "https://api.giphy.com/v1/gifs/search?" +
our api key "api_key=fzPJXS83lvCORZBSv9AUoGYX3WiC3gS3"*/
let query;
let request;
let ourfont;

function preload(){
  ourfont=loadFont('font.ttf');
}

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(200,200,250);

  textSize(20);
  textFont(ourfont);
  text("hi, type something to explore the Giphy database",60,70)
  let col1=color(100,250,150);
  let col2=color(250);
  input = createInput();//the input box where the user types
  input.position(135, 105);
  input.size(150,30);
  input.style('background-color',col2);
  input.input(gotData);

  button = createButton('search gif');
  button.position(300, 105);
  button.mousePressed(gotData);
  button.size(80,35)
  button.style('background-color',col1);
}

function gotData(giphy){
  query=input.value();/*we wanted the query to be whatever the user writes,
  and stated this here by defining the query as the input value*/
  request = url + "&q=" + query;/*putting our url (search+api key) and the query
  together for the request*/

  loadJSON(request,gotData);

  let img1=createImg(giphy.data[1].images.original.url);
  img1.position(450,80);
  img1.size(220,180);
  let img2=createImg(giphy.data[2].images.original.url);
  img2.position(280,300);
  img2.size(300,200);
  let img3=createImg(giphy.data[3].images.original.url);
  img3.position(100,250);
  img3.size(200,150);
  /*we wanted three gifs to appear in total, and decided that we would like the
  1st, 2nd and 3rd in the array, defined by the giphy.data[number].images.original.url*/
}
