let youngmangif;//the young man
let grandpagif;//the grandpa/old man
let runnergif;//the runner
let people=[];

let flower;
let you;//"your" character - she is facing the people
let youSize={
  w:150,
  h:200
};
let min_people=3;
let score=0;

function preload(){
  youngmangif=loadImage("youngm.gif");
  grandpagif=loadImage("grandpa.gif");
  runnergif=loadImage("runner.gif");
  you=loadImage("you.gif");
  flower=loadImage("flower.png");
}
function setup(){
  createCanvas(windowWidth,windowHeight);
}
function draw(){
  background(70,200,70);
  image(flower,40,10,30,30);
  image(flower,200,500,30,30);
  image(flower,500,100,30,30);
  image(flower,350,250,30,30);
  image(flower,650,450,30,30);

  checkNumber();
  showPeople();
  checkMovement();
  socialDistance();
  showScore();

  noStroke();
  fill(255,255,255,50);
  ellipse(100,mouseY+70,100,200);
  image(you,0,mouseY,youSize.h,youSize.w);/*I want the user to control the character
  with the mouse's y position*/
}
function checkNumber(){//this function gets the different classes
  if(people.length<min_people){
    people.push(new Youngman());//takes from the youngman class and sketch
    people.push(new Grandpa());//takes from the grandpa class and sketch
    people.push(new Runner());//takes from the runner class and sketch
  }
}
function showPeople(){//this function shows the people on the screen
  for(let i=0;i<people.length;i++){
    people[i].move();
    people[i].show();
  }
}
function checkMovement(){/*this function makes sure that when the people reaches the
border of the window, more people appear*/
  for(let i=0;i<people.length;i++)
  if(people[i].pos.x<3){
    people.splice(i,1);
  }
}
function socialDistance(){
  for(let i=0;i<people.length;i++){
  let d=int(
    dist(youSize.w,mouseY+youSize.h/2,
    people[i].pos.x,people[i].pos.y)
  );
  if(d<youSize.w/1.5){//If the people are within your character's width divided by 1.5, the people disappear and the score does not change.
    people.splice(i,1);
  }else if(people[i].pos.x<10){//If the people arrive at an x-position that is less than 10, the score goes up.
    score++;
    people.splice(i,1);
  }
  }
}
function showScore(){
  fill(250,20,150);
  textSize(20);
  text("You have kept a safe distance from "+score+" people. Good job!",100,80);
}
