### MINIX7 - Game with objects (OOP)

**[RunMe - The Social Distance Game](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix7)**

<img src="minix7screenshot.png" width="500">

[Code repository](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix's/minix7/sketch.js) +
_[Young man class,](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix's/minix7/youngman.js)
[Grandpa class,](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix's/minix7/grandpa.js)
[Runner class](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix's/minix7/runner.js)_


_This week we have learned about Object Oriented Programming, and we were given the task to create a game through OOP. My little game is based on the tofu game, but here the task is to avoid people (social distancing). This week I again had a very different vision (and too high ambitions), but after coding frustration throughout the weekend, I decided to start over and create this game. I also wanted to add a few other things in this game, but I couldn't figure it out and in the end I gave a little bit up. Therefore the game is very simple. However, I like the visual aspect of it, I think it kinda cute :)_

---

**Describe how does/do your game/game objects work?**

In my game you play as the character facing right; a happy girl who's taking a walk during covid and wishes to keep her distance from the other people outside. However, there are many people outside, as the weather is good! The people range from different age groups: we have the young runner, who is running in a high speed and does not want to be stopped; we have the old and grumpy man, he is slow and is especially afraid of the virus; and we have a teenager, who seem to not have a worry in his life.

<img src="you.gif" width="200">
<img src="grandpa.gif" width="200">
<img src="runner.gif" width="200">
<img src="youngm.gif" width="200">

_(I've borrowed these amazing gifs from giphy.com. From left: The "you" character, the grandpa, the runner and the young man)_

The goal is to keep your social distance, and as you avoid more people, your score goes up! Let's encourage social distancing and a joint effort of keeping the virus from spreading :)

**Describe how you program the objects and their related attributes, and the
methods in your game.**

I started by thinking of which kind of people(objects) I wanted to incorporate, and quickly decided on the three. I wanted different age groups and looks, as we can relate to them in real life. There it made sense to speed up the runner and slow down the old grandpa for example. By using gifs, I also think the objects look more realistic, especially as the objects are moving. If I were to develop the game further, I would probably give the classes more properties and behaviours, to specify and differentiate more between them.

- Classes: Youngman, Grandpa and Runner.
- Properties: `image`, `speed`, `size` and `positions`
- Behaviours: `show` and movement (right to left), in addition to rules about when to reappear and disappear


**Draw upon the assigned reading, what are the characteristics of object-oriented
programming and the wider implications of abstraction?**

As the objects and their relation to other matter is in focus in OOP, I think it is very important to create thought-through characteristics (properties and behaviours), especially when creating a game or a program that's supposed to be life-like or realistic. When programming objects that are taken from real life, it is inevitable that some operationss and details will be left out, as Soon and Cox talks about in the AP book (145). I think OOP sheds a light on this matter, and I think a programmer will have the possibility to reflect more upon the conscious choices of how an object is going to be presented and represented when using an object oriented approach.

**– Connect your game project to a wider cultural context, and think of an example
to describe how complex details and operations are being “abstracted”?**

My program is related to the pandemic and social distancing. As we have looked at many examples of games where the object is to catch something, my program plays with avoiding something instead. My program is also meant to encourage a the behaviour (social distancing) by giving points, instead of stating an end of the game - where the user looses, and I think when dealing with a sensitive subject, such as covid, the program benefits from having a positive theme.Although nowadays the game is totally understandable (and relatable), just a year and a half ago, social distancing would have sounded crazy and even rude. Avoiding human contact is such an unatural behaviour for humans, and getting points for how many people you manage to avoid sounds absurd. However, as the world learns and import new behaviours, programming does too. OOP revolves around the object and takes inspiration from the real world. In this way I think programming can develop and incorporate important aspects in a natural way.

In my program I actually try to represent several different people with different age, gender and condition. It is a very simplified game, that does not actively draw any negativity towards any of the people, and only incorporates very basic properties such as walking/running speed. The gifs portray some characteristics - for example the slow walk with walking chair for the old man and a slouchy posture for the young man, but does not include much else. Maybe more importantly, the program does not include any visible properties or behaviours that links directly to the pandemic, which is the whole theme of the game. When reflecting upon it, one might be critical to the choice of an old person or the fact that none of the people are wearing masks. Even though the game is very simplified, I could also have incorporated a slowing down of the people's walk as they are approaching the "you" character, to mimic how one would act in real life, or add some spacing between the `min_humans` onscreen, as they can sometimes move very close to eachother.

---

### References:

- Old man gif: https://dribbble.com/shots/4296910-Old-Man-Walking 
- Young man gif: https://in.pinterest.com/pin/724446290056512034/
- Runner gif: https://giphy.com/gifs/THD7thMQZoOYoyZ3EK/html5
- You character gif: https://no.pinterest.com/pin/93449761001555425/
- Code repository for the Tofu game by Winnie Soon: https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch6_ObjectAbstraction/sketch.js
- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
- Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). (on blackboard\Literature)
