# Minix5: A generative program

[**My program (RunMe)**](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix5/)

[**Code repository**](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix5/sketch.js)

<img src="minix5video.mov" width="600">

# My program - "Organized streams of chaos"

**What are the rules in your generative program? Describe how your program
performs over time? How do the rules produce emergent behavior?**

<img src="currents.png" width="200">

For this weeks minix, I have taken inspiration from one of my favorite musician's album covers _(Tame Impala - Currents, pictured above)_, and incorporated different elements, like an image, ellipses, curves and movement. My program consits of two main "rules"(or set of rules):

1. `If()` statements for the silver floating ball (and surrounding circles), deciding the positions/limits for the movement, as well as direction and speed.
2. `For()` loops for the moving curves in the background, limiting and deciding the quantity. The curve elements themselves specifies the starting points, size etc., and here I have used `random()` functions to create the illusion of movement between the curves. Because they are also put underneath draw, instead of setup, they appear at the same time and again helps create the illusion. I've played around with colors and `strokeWeight()` specifically for the curves to create a more visually exciting program.

**What role do rules and processes have in your work?**

As I have decided to go for three streams of lines in random positions, the rules and framework for my program is important. It presents a more organized program, where chaotic "movement" is secured into a frame. By deciding starting/ending points in addition some form of freedom (for example `random(windowHeight)` for a y-position) for the curves, the contrast between the still background and the chaos-ness of the curves presents a visually pleasing aspect. The curves also appear to go faster than the silver ball, and this also creates a little more depth, and a nice effect, in my opinion.

**Draw upon the assigned reading, how does this MiniX help you to understand the
idea of “auto-generator” (e.g. levels of control, autonomy, love and care via
rules)? Do you have any further thoughts on the theme of this chapter?**


The idea of creating my own auto-generator seemed to be complex at first, but from reading and learning about especially 10-PRINT, I have come to see that it comes down to a set of rules. Thinking of an auto-generator as something that's forever producing more and more elements, it would seem as though it would be complex to contruct on my own, but especially when (finally) mastering the for-loop, you rule over so much with only a single line of code.

# References
- 10PRINT (https://10print.org/)
- Soon, Cox (2020). _Aesthetic Programming: A Handbook of Software Studies_ (http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf)
- The Coding Train 4.1 (https://www.youtube.com/watch?v=cnRD9o6odjk)
- Currents by Tame Impala (https://open.spotify.com/album/79dL7FLiJFOO0EoehUHQBv)
