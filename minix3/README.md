**MiniX 3 - THROBBER**

[Press here for RunME](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix3/) & [here for my code repository](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix3/sketch.js) :)

<img src="Screenshot_minix3.png" width="400">


1. For this weeks minix, I have made a ring spinning in a loop, and a finger going up and down touching it. Both the ring and the finger loops forever, and everytime the finger touches the ring a little "spark" appears. I wanted to explore movement this week - and while I learned the rings loop under Winnie's instruction, I learned the up and down movement of the finger by myself. I also wanted to continue to "draw" with shapes, and made it a slightly different style than I usually go for - with thicker black lines, and fewer details.

2. The time-related syntaxes I have used are `rotate(millis()/275)` for the ring(shapes and image), and different `if` statements for the finger. Time in my syntax is according to millisseconds, and I haven't used frameRate for this one. It was challenging to understand what time meant in this code and when looking at others, and I still have difficulties understanding the exact decider in time-related syntaxes. With the other minix's I learned quite fast, and I think I just need to give myself time to properly learn, instead of rushing it or only sticking to what I can learn quickly.

3. Its interesting how throbbers does not actually represent what they're commonly thought of as - a slow internet connection or generally other problems that does not concern us or our range of knowledge. Rather they're often put there to hide something, and diverts our attention. And since they are usually connected with an internet problem - the user does not blame the actual website, and therefore does not suspect any sketchy 'behind-the-scenes' action. They merely seem to be put there to annoy us, and I think that's what I thought of when I made my program. Usually I think of throbbers as the typical circular icon, like a snake trying to reach its tail, that has a task and goal that it'll never reach. In my program it is supposed to look like a finger that's trying to put the ring on, but whenever it touches it, the finger is disrupted and tries again. And while this goes on forever, the user can be tricked into thinking that there is progress.
