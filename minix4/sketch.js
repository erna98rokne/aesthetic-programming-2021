let capture;
let y=1;
let ball;
let tear;
let heart;
let hot;
let snd;

function preload(){
  ball=loadImage('ball.png');
  tear=loadImage('tear.png');
  heart=loadImage('heart.png');
  hot=loadImage('hot.png');
  snd=loadSound('madworld.mp3');
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(40);

//webcam capture
  capture=createCapture(VIDEO);
  capture.size(800, 600);
  capture.hide();

//the button
  let col=color(250,20,200,100);
  button1=createButton("click here my dude");
  button1.style('background-color',col);
  button1.position(400,450,20,20);
  button1.size(120,50);
  button1.mouseClicked(picturetime);
}
function draw() {
//draw the captured video on a screen with the image filter
  background(0);
  image(capture,20,120, 640, 480);
  image(capture,100,40, 640, 480);

//the ball
    y=y+10;
    if(y>460){
    y=10;
    }
    fill(20,250,20);
    noStroke();
    image(ball,430,y+30,40,40);

//the text
    fill(255);
    textSize(20);
    textFont('Helvetica');
    textStyle(ITALIC);
    text("click the button when the ball is in the middle of your mouth",105,30);
}
function picturetime(){
    frameRate(0); //freeze all the movement
    filter(GRAY); //put gray filter
    capture.stop(); //freeze the video
    button1.remove();
    snd.play();
    fill(0);
    rect(100,10,640,25);
    image(hot,95,7,23,23);
    image(heart,115,7,32,25);
    image(heart,505,7,32,25);
    image(hot,535,7,23,23);
    image(tear,325,170,120,120);
    image(tear,450,170,120,120);
    fill(255);
    textSize(30);
    textStyle(ITALIC);
    text("whats cookin' good lookin'",150,30);
}
