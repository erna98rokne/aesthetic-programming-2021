let smiley;
let heart;
let cowboy;
let cry;
let notification;

function preload() {
  smiley=loadImage('reversesmiley.png');
  heart=loadImage('heart.png');
  cowboy=loadImage('cowboy.png');
  cry=loadImage('sad.png');
  notification=loadSound('notification.mp3');
}
function setup() {
  createCanvas(2000,2000);
  frameRate(10);
  background(200,150,200);

//NORMAL FACE STARTS HERE
//EYES
  noFill();
  stroke(255);
  curve(50,130,215,133,225,110,100,100);//left eyecorner
  curve(150,150,130,125,158,120,200,150);//left eyewrinkle #1
  curve(150,150,140,133,158,120,200,150);//left eyewrinkle #2
  curve(150,150,148,133,158,120,200,150);//left eyewrinkle #3
  curve(400,100,365,130,360,110,500,100);//right eyecorner
  curve(400,150,455,125,430,120,450,150);//left eyewrinkle #3
  curve(400,150,450,133,430,120,450,150);//left eyewrinkle #2
  curve(400,150,440,133,430,120,450,150);//left eyewrinkle #1
  arc(188,145,50,15,0,PI,TWO_PI);//left undereye
  arc(400,145,50,15,0,PI,TWO_PI);//right undereye
  fill(170,120,170);
  arc(188,120,60,50,0,PI,TWO_PI);//left eye
  arc(400,120,60,50,0,PI,TWO_PI);//right eye
//EYEBROWS
  arc(400,80,150,30,0,PI,OPEN);//right eyebrow
  curve(40,30,100,110,270,60,400,400);//left eyebrow
  noFill();
  curve(400,200,305,90,305,55,300,20);//eyebrow wrinkle
//NOSE & NOSELINES
  arc(265,248,30,23,0,PI+QUARTER_PI,OPEN);//left nostril
  arc(300,250,40,28,0,PI,OPEN);//nosetip
  arc(335,248,30,20,0,PI,OPEN);//right nostril
  curve(320,200,280,240,300,215,400,250);//noseline #1
  curve(400,200,310,210,315,100,200,20); //noseline #2
//SMILE & WRINKLES
  curve(40,200,155,305,250,235,400,300); //left smilewrinkle
  curve(550,200,440,305,350,235,80,300); //right smilewrinkle
  curve(200,300,130,315,145,285,10,200);//left dimple
  curve(400,400,450,315,420,285,400,400);//right dimple
  line(210,290,185,310);//mouth line
  line(390,290,415,310);//mouth line
  arc(300,445,80,20,PI,TWO_PI);//chin line
  fill(170,120,170);
  rect(200,320,200,80,0,0,55,55);//lower lip
  arc(300,300,200,40,PI,TWO_PI);//upper lip
  fill(150,100,150);
  rect(200,300,200,75,0,0,55,55);//mouth
  noStroke();
  fill(180,130,180);
  ellipse(300,470,75,55);//shadow chin
  fill(250,200,250);
  ellipse(300,460,75,35);//highlight chin
  fill(170,120,170);
  stroke(255);
  arc(320,375,60,40,PI,TWO_PI);//tongue
  arc(280,375,60,40,PI,TWO_PI);//tongue
  arc(300,375,100,150,0,PI,TWO_PI);//tongue
  noFill();
  curve(-100,350,125,350,70,170,300,20); //left cheekline
  curve(700,350,460,350,520,170,300,20); //right cheekline
  arc(150,180,60,15,PI,TWO_PI);//upper cheek line
  arc(450,180,60,15,PI,TWO_PI);//upper cheek line
  arc(150,240,100,40,0,PI,TWO_PI);//lower cheek line
  arc(450,240,100,40,0,PI,TWO_PI);//lower cheek line
//TEETH
  fill(220,170,220);
  rect(272,300,28,30,0,0,2,2);//inner teeth
  rect(300,300,28,30,0,0,2,2);//inner teeth
  fill(200,150,200);
  rect(328,300,20,25,0,0,2,2);//teeth
  rect(252,300,20,25,0,0,2,2);//teeth
  fill(180,130,180);
  rect(348,300,12,23,0,0,2,2);//teeth
  rect(240,300,12,23,0,0,2,2);//teeth
  fill(160,110,160);
  rect(228,300,12,20,0,0,2,2);//outer teeth
  rect(360,300,12,20,0,0,2,2);//outer teeth
//HIGHLIGHTS & SHADOWS
  fill(180,130,180);
  rect(200,510,200,2000);//neck
  fill(160,110,160);
  noStroke();
  rect(200,505,200,70,0,0,55,55);//neck shadow
  fill(200,150,200);
  stroke(255);
  arc(300,500,250,60,0,PI,TWO_PI);//chin
  fill(180,130,180);
  noStroke();
  ellipse(150,220,90,70);//shadow cheek
  ellipse(450,220,90,70)//shadow cheek
  fill(250,200,250);
  ellipse(300,130,25,70);//highlight noseline
  ellipse(300,247,27,21);//highlight nosetip
  ellipse(150,210,90,50);//highlight left cheek
  ellipse(450,210,90,50);//highlight right cheek
  fill(190,140,190);
  ellipse(300,290,50,10);//highlight upper lip
  ellipse(285,370,30,20);//highlight tongue
  ellipse(315,370,30,20);//highlight tongue
  fill(210,160,210);
  ellipse(300,425,35,20);//highlight tongue
  fill(160,110,160);
  ellipse(300,392,50,25);//shadow tongue
  stroke(255);
  line(300,360,300,410);//line tongue
  noStroke();
  fill(180,130,180);
  ellipse(297,190,15,30);//shadow noseline
  ellipse(175,350,30,60);//shadow left mouth
  ellipse(423,350,30,60);//shadow right mouth
  fill(135,85,135);
  ellipse(300,345,100,20);//shadow inside mouth
}
function draw() {
  let w="click the screen & buttons to make me express my emotions in the modern world"
  stroke(255);
  textSize(35);
  fill(random(100,200),random(100,200),random(100,200));
  text(w,540,50,400,300);
}
function mousePressed() {
  fill(180,130,180);
  stroke(255);
  rect(600,490,100,2000);//arm
  fill(155,105,155);
  noStroke();
  rect(600,500,100,70,0,0,25,45);//arm shadow
  fill(200,150,200);
  stroke(255);
  rect(550,430,180,100,55,55,35,55);//hand
  arc(567,470,30,230,PI,TWO_PI);//thumb
  fill(100);
  rect(570,260,170,240,15,15,15,15);//phone
  fill(200);
  rect(580,270,150,220,15,15,15,15);//screen
  fill(200,150,200);
  rect(730,380,25,30,55,10,10,55);//finger upper
  rect(710,430,40,30,55,10,10,55);//finger lower
  fill(250,200,250);
  rect(735,385,20,20,55,5,5,55);//fingernail upper
  rect(725,435,25,20,55,5,5,55);//fingernail lower
  arc(560,395,9,65,PI,TWO_PI);//fingernail thumb
//TEXT
  let w2="which emotion do I feel today?"
  stroke(255);
  textSize(20);
  fill(100);
  text(w2,750,220,140,100);
//BUTTONS
  love=createButton("I feel love");
  love.position(770,300);
  love.mousePressed(lovesketch);
  happy=createButton("I feel happy");
  happy.position(770,330);
  happy.mousePressed(happysketch);
  sad=createButton("I feel sad");
  sad.position(770,360);
  sad.mousePressed(sadsketch);
  normal=createButton("I feel normal");
  normal.position(770,390);
  normal.mousePressed(normalsketch);
}
//HERE STARTS THE NEW SKETCHES (AKA THE DIFFERENT EMOTIONS)
/*They are basically the same as the "normal" sketch, but with different
placements of the shapes. Be prepared for many lines of code ahead...*/

//FIRST EMOTION - LOVE
function lovesketch() {
    createCanvas(2000,2000);
    background(200,150,200);
  //GENERAL FACE
    stroke(255);
    noFill();
    curve(-100,350,125,350,70,170,300,20); //left cheekline
    curve(700,350,460,350,520,170,300,20); //right cheekline
    arc(150,180,60,15,PI,TWO_PI);//upper cheek line
    arc(450,180,60,15,PI,TWO_PI);//upper cheek line
    arc(150,240,100,40,0,PI,TWO_PI);//lower cheek line
    arc(450,240,100,40,0,PI,TWO_PI);//lower cheek line
  //EYES
    curve(50,130,215,133,225,110,100,100);//left eyecorner
    curve(150,150,130,125,158,120,200,150);//left eyewrinkle #1
    curve(150,150,140,133,158,120,200,150);//left eyewrinkle #2
    curve(150,150,148,133,158,120,200,150);//left eyewrinkle #3
    curve(400,100,365,130,360,110,500,100);//right eyecorner
    curve(400,150,455,125,430,120,450,150);//left eyewrinkle #3
    curve(400,150,450,133,430,120,450,150);//left eyewrinkle #2
    curve(400,150,440,133,430,120,450,150);//left eyewrinkle #1
    arc(188,145,50,15,0,PI,TWO_PI);//left undereye
    arc(400,145,50,15,0,PI,TWO_PI);//right undereye
    stroke(180,130,180);
    fill(250,200,250);
    arc(188,130,60,30,0,PI,TWO_PI);//left eye
    arc(400,130,60,30,0,PI,TWO_PI);//right eye
    arc(188,130,60,50,PI,TWO_PI);//left eye
    arc(400,130,60,50,PI,TWO_PI);//right eye
    fill(255);
    noStroke();
    ellipse(188,125,50,30);
    ellipse(400,125,50,30);
  //EYEBROWS
    noStroke();
    fill(135,85,135);
    triangle(170, 125, 190, 150, 210, 125);//heart left eye
    arc(180,125,20,40,PI,TWO_PI);//heart left eye
    arc(200,125,20,40,PI,TWO_PI);//heart left eye
    triangle(380, 125, 400, 150, 420, 125);//heart right eye
    arc(390,125,20,40,PI,TWO_PI);//heart right eye
    arc(410,125,20,40,PI,TWO_PI);//heart right eye
    stroke(255);
    fill(170,120,170);
    curve(600,30,490,110,330,60,400,400);//right eyebrow
    curve(40,30,100,110,270,60,400,400);//left eyebrow
    noFill();
    curve(400,200,305,90,305,55,300,20);//eyebrow wrinkle
  //NOSE & NOSELINES
    arc(265,248,30,23,0,PI+QUARTER_PI,OPEN);//right nesebor
    arc(300,250,40,28,0,PI,OPEN);//nosetip
    arc(335,248,30,20,0,PI,OPEN);//right nesebor
    curve(320,200,280,240,300,215,400,250);//noseline#2
    curve(400,200,310,210,315,100,200,20); //noseline
  //SMILE & WRINKLES
    noFill();
    curve(40,200,155,305,250,235,400,300); //left smilewrinkle
    curve(550,200,440,305,350,235,80,300); //right smilewrinkle
    curve(200,300,130,315,145,285,10,200);//left dimple
    curve(400,400,450,315,420,285,400,400);//right dimple
    line(210,290,185,310);//mouth line
    line(390,290,415,310);//mouth line
    arc(300,445,80,20,PI,TWO_PI);//chin line
    fill(170,120,170);
    arc(280,330,70,80,PI,TWO_PI);//upper lip
    arc(320,330,70,80,PI+QUARTER_PI,TWO_PI);//upper lip
    rect(250,330,100,50,0,0,55,55);//lower lip
    fill(135,85,135);
    arc(300,330,40,20,PI,TWO_PI);//inside mouth
    arc(300,330,40,40,0,PI,TWO_PI);//inside mouth
  //HIGHLIGHTS & SHADOWS
    fill(180,130,180);
    stroke(255);
    rect(200,490,200,2000);//neck
    fill(160,110,160);
    noStroke();
    rect(200,490,200,70,0,0,55,55);//neck shadow
    fill(200,150,200);
    stroke(255);
    arc(300,480,250,70,0,PI,TWO_PI);//chin
    fill(180,130,180);
    noStroke();
    ellipse(150,220,90,70);//shadow cheek
    ellipse(450,220,90,70)//shadow cheek
    ellipse(300,470,75,55);//shadow chin
    fill(250,200,250);
    ellipse(300,130,25,70);//highlight noseline
    ellipse(300,247,27,21);//highlight nosetip
    ellipse(150,210,90,50);//highlight left cheek
    ellipse(450,210,90,50);//highlight right cheek
    ellipse(300,460,75,35);//highlight chin
    fill(190,140,190);
    ellipse(300,310,50,10);//highlight upper lip
    ellipse(300,365,50,15);//highlight lower lip
    fill(180,130,180);
    ellipse(297,190,15,30);//shadow noseline
    ellipse(175,350,30,60);//shadow left mouth
    ellipse(423,350,30,60);//shadow right mouth

  //ARM
    fill(180,130,180);
    stroke(255);
    rect(600,490,100,2000);//arm
    fill(155,105,155);
    noStroke();
    rect(600,500,100,70,0,0,25,45);//arm shadow
    fill(200,150,200);
    stroke(255);
    rect(550,430,180,100,55,55,35,55);//hand
    arc(567,470,30,230,PI,TWO_PI);//thumb
    fill(100);
    rect(570,260,170,240,15,15,15,15);//phone
    fill(200);
    rect(580,270,150,220,15,15,15,15);//screen
    fill(200,150,200);
    rect(730,380,25,30,55,10,10,55);//finger upper
    rect(710,430,40,30,55,10,10,55);//finger lower
    fill(250,200,250);
    rect(735,385,20,20,55,5,5,55);//fingernail upper
    rect(725,435,25,20,55,5,5,55);//fingernail lower
    arc(560,395,9,65,PI,TWO_PI);//fingernail thumb

  //EMOJIS
    fill(250,200,250);
    noStroke();
    rect(790,440,160,90);
    triangle(765,450,790,470,790,450);
    image(heart,810,470,45,35);
    image(heart,852,470,45,35);
    image(heart,895,470,45,35);
    notification.play();
}
//SECOND EMOTION - HAPPY
function happysketch() {
  happycnv=createCanvas(2000,2000);
  background(200,150,200);
//GENERAL FACE
  stroke(255);
  noFill();
  curve(-100,350,125,350,70,170,300,20); //left cheekline
  curve(700,350,460,350,520,170,300,20); //right cheekline
  arc(150,180,60,15,PI,TWO_PI);//upper cheek line
  arc(450,180,60,15,PI,TWO_PI);//upper cheek line
  arc(150,240,100,40,0,PI,TWO_PI);//lower cheek line
  arc(450,240,100,40,0,PI,TWO_PI);//lower cheek line

//EYES
  curve(50,130,215,133,225,110,100,100);//left eyecorner
  curve(150,150,130,125,158,120,200,150);//left eyewrinkle #1
  curve(150,150,140,133,158,120,200,150);//left eyewrinkle #2
  curve(150,150,148,133,158,120,200,150);//left eyewrinkle #3
  curve(400,100,365,130,360,110,500,100);//right eyecorner
  curve(400,150,455,125,430,120,450,150);//left eyewrinkle #3
  curve(400,150,450,133,430,120,450,150);//left eyewrinkle #2
  curve(400,150,440,133,430,120,450,150);//left eyewrinkle #1
  arc(188,145,50,15,0,PI,TWO_PI);//left undereye
  arc(400,145,50,15,0,PI,TWO_PI);//right undereye
  stroke(255);
  arc(188,120,60,30,0,PI,TWO_PI);//left eye
  arc(400,120,60,30,0,PI,TWO_PI);//right eye

//EYEBROWS
  fill(170,120,170);
  curve(40,30,100,110,270,60,400,400);//left eyebrow
  curve(600,30,490,110,320,40,400,400);//right eyebrow
  noFill();
  curve(400,200,305,90,305,55,300,20);//eyebrow wrinkle

//NOSE & NOSELINES
  arc(265,248,30,23,0,PI+QUARTER_PI,OPEN);//right nesebor
  arc(300,250,40,28,0,PI,OPEN);//nosetip
  arc(335,248,30,20,0,PI,OPEN);//right nesebor
  curve(320,200,280,240,300,215,400,250);//noseline#2
  curve(400,200,310,210,315,100,200,20); //noseline

//SMILE & WRINKLES
  noFill();
  curve(40,200,155,305,250,235,400,300); //left smilewrinkle
  curve(550,200,440,305,350,235,80,300); //right smilewrinkle
  curve(200,300,130,315,145,285,10,200);//left dimple
  curve(400,400,450,315,420,285,400,400);//right dimple
  line(210,290,185,310);//mouth line
  line(390,290,415,310);//mouth line
  arc(300,445,80,20,PI,TWO_PI);//chin line
  fill(170,120,170);
  rect(200,320,200,100,0,0,55,55);//lower lip
  arc(300,300,200,40,PI,TWO_PI);//upper lip
  fill(150,100,150);
  rect(200,300,200,100,0,0,55,55);//mouth
  fill(170,120,170);
  arc(315,400,50,40,PI,TWO_PI);//tongue
  arc(285,400,50,40,PI,TWO_PI);//tongue

//TEETH
  fill(220,170,220);
  rect(272,300,28,30,0,0,2,2);//inner upper teeth
  rect(300,300,28,30,0,0,2,2);//inner upper teeth
  fill(200,150,200);
  rect(328,300,20,25,0,0,2,2);
  rect(252,300,20,25,0,0,2,2);
  fill(180,130,180);
  rect(348,300,12,23,0,0,2,2);
  rect(240,300,12,23,0,0,2,2);
  fill(160,110,160);
  rect(228,300,12,20,0,0,2,2);//outer upper teeth
  rect(360,300,12,20,0,0,2,2);//outer upper teeth
  rect(245,393,15,7,5,5,0,0);//outer lower teeth
  rect(340,393,15,7,5,5,0,0);//outer lower teeth
  fill(180,130,180);
  rect(325,391,15,9,5,5,0,0);
  rect(260,391,15,9,5,5,0,0);
  fill(220,170,220);
  rect(301,390,23,10,5,5,0,0);//inner lower teeth
  rect(276,390,23,10,5,5,0,0);//inner lower teeth

//HIGHLIGHTS & SHADOWS
  fill(180,130,180);
  stroke(255);
  rect(200,490,200,2000);//neck
  fill(160,110,160);
  noStroke();
  rect(200,490,200,70,0,0,55,55);//neck shadow
  fill(200,150,200);
  stroke(255);
  arc(300,480,250,70,0,PI,TWO_PI);//chin
  fill(180,130,180);
  noStroke();
  ellipse(150,220,90,70);//shadow cheek
  ellipse(450,220,90,70)//shadow cheek
  ellipse(300,470,75,55);//shadow chin
  fill(250,200,250);
  ellipse(300,130,25,70);//highlight noseline
  ellipse(300,247,27,21);//highlight nosetip
  ellipse(150,210,90,50);//highlight left cheek
  ellipse(450,210,90,50);//highlight right cheek
  ellipse(300,460,75,35);//highlight chin
  fill(190,140,190);
  ellipse(300,290,50,10);//highlight upper lip
  ellipse(300,410,80,10);//highlight lower lip
  fill(180,130,180);
  ellipse(297,190,15,30);//shadow noseline
  ellipse(175,350,30,60);//shadow left mouth
  ellipse(423,350,30,60);//shadow right mouth
  fill(135,85,135);
  ellipse(300,355,100,40);//shadow inside mouth

//ARM
fill(180,130,180);
stroke(255);
rect(600,490,100,2000);//arm
fill(155,105,155);
noStroke();
rect(600,500,100,70,0,0,25,45);//arm shadow
fill(200,150,200);
stroke(255);
rect(550,430,180,100,55,55,35,55);//hand
arc(567,470,30,230,PI,TWO_PI);//thumb
fill(100);
rect(570,260,170,240,15,15,15,15);//phone
fill(200);
rect(580,270,150,220,15,15,15,15);//screen
fill(200,150,200);
rect(730,380,25,30,55,10,10,55);//finger upper
rect(710,430,40,30,55,10,10,55);//finger lower
fill(250,200,250);
rect(735,385,20,20,55,5,5,55);//fingernail upper
rect(725,435,25,20,55,5,5,55);//fingernail lower
arc(560,395,9,65,PI,TWO_PI);//fingernail thumb

//EMOJIS
  fill(250,200,250);
  noStroke();
  rect(790,440,160,90);
  triangle(765,450,790,470,790,450);
  image(cowboy,810,470,35,35);
  image(cowboy,852,470,35,35);
  image(cowboy,895,470,35,35);
  notification.play();
}
function sadsketch() {
  createCanvas(2000,2000);
  background(200,150,200);
  //GENERAL FACE
  noFill();
  stroke(255);
  curve(50,130,215,133,225,110,100,100);//left eyecorner
  curve(150,150,130,125,158,120,200,150);//left eyewrinkle #1
  curve(150,150,140,133,158,120,200,150);//left eyewrinkle #2
  curve(150,150,148,133,158,120,200,150);//left eyewrinkle #3
  curve(400,100,365,130,360,110,500,100);//right eyecorner
  curve(400,150,455,125,430,120,450,150);//left eyewrinkle #3
  curve(400,150,450,133,430,120,450,150);//left eyewrinkle #2
  curve(400,150,440,133,430,120,450,150);//left eyewrinkle #1
  arc(188,145,50,15,0,PI,TWO_PI);//left undereye
  arc(400,145,50,15,0,PI,TWO_PI);//right undereye
  arc(188,120,60,30,0,PI,TWO_PI);//left eye
  arc(188,138,60,5,PI,TWO_PI);//left eye
  arc(400,120,60,30,0,PI,TWO_PI);//right eye
  arc(400,138,60,5,PI,TWO_PI);//right eye
  //EYEBROWS
  fill(170,120,170);
  curve(40,0,100,120,290,60,400,400);//left eyebrow
  curve(300,400,310,50,480,120,400,00);//left eyebrow
  noFill();
  curve(400,200,305,70,305,30,300,20);//eyebrow wrinkle
  //NOSE & NOSELINES
  arc(265,248,30,23,0,PI+QUARTER_PI,OPEN);//left nostril
  arc(300,250,40,28,0,PI,OPEN);//nosetip
  arc(335,248,30,20,0,PI,OPEN);//right nostril
  curve(320,200,280,240,300,215,400,250);//noseline #1
  curve(400,200,310,210,315,100,200,20); //noseline #2
  //SMILE & WRINKLES
  curve(40,200,155,305,250,235,400,300); //left smilewrinkle
  curve(550,200,440,305,350,235,80,300); //right smilewrinkle
  curve(200,300,130,315,145,285,10,200);//left dimple
  curve(400,400,450,315,420,285,400,400);//right dimple
  line(210,290,185,310);//mouth line
  line(390,290,415,310);//mouth line
  arc(300,445,80,20,PI,TWO_PI);//chin line
  fill(170,120,170);
  rect(200,320,200,90,55,55,20,20);//lower lip
  arc(260,335,120,110,PI,TWO_PI);//upper lip
  arc(340,335,120,110,PI,TWO_PI);
  fill(150,100,150);
  rect(200,300,200,90,55,55,0,0);//mouth
  noStroke();
  fill(180,130,180);
  ellipse(300,470,75,55);//shadow chin
  fill(250,200,250);
  ellipse(300,460,75,35);//highlight chin
  fill(170,120,170);
  stroke(255);
  noFill();
  curve(-100,350,125,350,70,170,300,20); //left cheekline
  curve(700,350,460,350,520,170,300,20); //right cheekline
  arc(150,180,60,15,PI,TWO_PI);//upper cheek line
  arc(450,180,60,15,PI,TWO_PI);//upper cheek line
  arc(150,240,100,40,0,PI,TWO_PI);//lower cheek line
  arc(450,240,100,40,0,PI,TWO_PI);//lower cheek line
  //TEETH
  fill(220,170,220);
  rect(272,300,28,30,0,0,2,2);//inner teeth
  rect(300,300,28,30,0,0,2,2);//inner teeth
  fill(200,150,200);
  rect(328,300,20,25,0,0,2,2);//teeth
  rect(252,300,20,25,0,0,2,2);//teeth
  fill(180,130,180);
  rect(348,300,12,23,0,0,2,2);//teeth
  rect(240,300,12,23,0,0,2,2);//teeth
  fill(160,110,160);
  rect(228,302,12,20,2,0,2,2);//outer teeth
  rect(360,302,12,20,0,2,2,2);//outer teeth
  //HIGHLIGHTS & SHADOWS
  fill(180,130,180);
  rect(200,510,200,2000);//neck
  fill(160,110,160);
  noStroke();
  rect(200,505,200,70,0,0,55,55);//neck shadow
  fill(200,150,200);
  stroke(255);
  arc(300,500,250,60,0,PI,TWO_PI);//chin
  fill(180,130,180);
  noStroke();
  ellipse(150,220,90,70);//shadow cheek
  ellipse(450,220,90,70)//shadow cheek
  fill(250,200,250);
  ellipse(300,130,25,70);//highlight noseline
  ellipse(300,247,27,21);//highlight nosetip
  ellipse(150,210,90,50);//highlight left cheek
  ellipse(450,210,90,50);//highlight right cheek
  fill(190,140,190);
  ellipse(260,290,40,10);//highlight upper lip
  ellipse(340,290,40,10);//highlight upper lip
  ellipse(300,400,70,10);//highlight lower lip
  noStroke();
  fill(180,130,180);
  ellipse(297,190,15,30);//shadow noseline
  ellipse(175,350,30,60);//shadow left mouth
  ellipse(423,350,30,60);//shadow right mouth
  fill(135,85,135);
  ellipse(300,360,120,40);//shadow inside mouth
//EMOJIES
  fill(250,200,250);
  noStroke();
  rect(790,440,160,90);
  triangle(765,450,790,470,790,450);
  image(cry,810,470,35,35);
  image(cry,852,470,35,35);
  image(cry,895,470,35,35);
  notification.play();
}
//THE NORMAL EMOTION AGAIN...
function normalsketch() {
  createCanvas(2000,2000);
  background(200,150,200);
  //EYES
  noFill();
  stroke(255);
  curve(50,130,215,133,225,110,100,100);//left eyecorner
  curve(150,150,130,125,158,120,200,150);//left eyewrinkle #1
  curve(150,150,140,133,158,120,200,150);//left eyewrinkle #2
  curve(150,150,148,133,158,120,200,150);//left eyewrinkle #3
  curve(400,100,365,130,360,110,500,100);//right eyecorner
  curve(400,150,455,125,430,120,450,150);//left eyewrinkle #3
  curve(400,150,450,133,430,120,450,150);//left eyewrinkle #2
  curve(400,150,440,133,430,120,450,150);//left eyewrinkle #1
  arc(188,145,50,15,0,PI,TWO_PI);//left undereye
  arc(400,145,50,15,0,PI,TWO_PI);//right undereye
  fill(170,120,170);
  arc(188,120,60,50,0,PI,TWO_PI);//left eye
  arc(400,120,60,50,0,PI,TWO_PI);//right eye
  //EYEBROWS
  arc(400,80,150,30,0,PI,OPEN);//right eyebrow
  curve(40,30,100,110,270,60,400,400);//left eyebrow
  noFill();
  curve(400,200,305,90,305,55,300,20);//eyebrow wrinkle
  //NOSE & NOSELINES
  arc(265,248,30,23,0,PI+QUARTER_PI,OPEN);//left nostril
  arc(300,250,40,28,0,PI,OPEN);//nosetip
  arc(335,248,30,20,0,PI,OPEN);//right nostril
  curve(320,200,280,240,300,215,400,250);//noseline #1
  curve(400,200,310,210,315,100,200,20); //noseline #2
  //SMILE & WRINKLES
  curve(40,200,155,305,250,235,400,300); //left smilewrinkle
  curve(550,200,440,305,350,235,80,300); //right smilewrinkle
  curve(200,300,130,315,145,285,10,200);//left dimple
  curve(400,400,450,315,420,285,400,400);//right dimple
  line(210,290,185,310);//mouth line
  line(390,290,415,310);//mouth line
  arc(300,445,80,20,PI,TWO_PI);//chin line
  fill(170,120,170);
  rect(200,320,200,80,0,0,55,55);//lower lip
  arc(300,300,200,40,PI,TWO_PI);//upper lip
  fill(150,100,150);
  rect(200,300,200,75,0,0,55,55);//mouth
  noStroke();
  fill(180,130,180);
  ellipse(300,470,75,55);//shadow chin
  fill(250,200,250);
  ellipse(300,460,75,35);//highlight chin
  fill(170,120,170);
  stroke(255);
  arc(320,375,60,40,PI,TWO_PI);//tongue
  arc(280,375,60,40,PI,TWO_PI);//tongue
  arc(300,375,100,150,0,PI,TWO_PI);//tongue
  noFill();
  curve(-100,350,125,350,70,170,300,20); //left cheekline
  curve(700,350,460,350,520,170,300,20); //right cheekline
  arc(150,180,60,15,PI,TWO_PI);//upper cheek line
  arc(450,180,60,15,PI,TWO_PI);//upper cheek line
  arc(150,240,100,40,0,PI,TWO_PI);//lower cheek line
  arc(450,240,100,40,0,PI,TWO_PI);//lower cheek line
  //TEETH
  fill(220,170,220);
  rect(272,300,28,30,0,0,2,2);//inner teeth
  rect(300,300,28,30,0,0,2,2);//inner teeth
  fill(200,150,200);
  rect(328,300,20,25,0,0,2,2);//teeth
  rect(252,300,20,25,0,0,2,2);//teeth
  fill(180,130,180);
  rect(348,300,12,23,0,0,2,2);//teeth
  rect(240,300,12,23,0,0,2,2);//teeth
  fill(160,110,160);
  rect(228,300,12,20,0,0,2,2);//outer teeth
  rect(360,300,12,20,0,0,2,2);//outer teeth
  //HIGHLIGHTS & SHADOWS
  fill(180,130,180);
  rect(200,510,200,2000);//neck
  fill(160,110,160);
  noStroke();
  rect(200,505,200,70,0,0,55,55);//neck shadow
  fill(200,150,200);
  stroke(255);
  arc(300,500,250,60,0,PI,TWO_PI);//chin
  fill(180,130,180);
  noStroke();
  ellipse(150,220,90,70);//shadow cheek
  ellipse(450,220,90,70)//shadow cheek
  fill(250,200,250);
  ellipse(300,130,25,70);//highlight noseline
  ellipse(300,247,27,21);//highlight nosetip
  ellipse(150,210,90,50);//highlight left cheek
  ellipse(450,210,90,50);//highlight right cheek
  fill(190,140,190);
  ellipse(300,290,50,10);//highlight upper lip
  ellipse(285,370,30,20);//highlight tongue
  ellipse(315,370,30,20);//highlight tongue
  fill(210,160,210);
  ellipse(300,425,35,20);//highlight tongue
  fill(160,110,160);
  ellipse(300,392,50,25);//shadow tongue
  stroke(255);
  line(300,360,300,410);//line tongue
  noStroke();
  fill(180,130,180);
  ellipse(297,190,15,30);//shadow noseline
  ellipse(175,350,30,60);//shadow left mouth
  ellipse(423,350,30,60);//shadow right mouth
  fill(135,85,135);
  ellipse(300,345,100,20);//shadow inside mouth
//EMOJIES
  fill(250,200,250);
  noStroke();
  rect(790,440,160,90);
  triangle(765,450,790,470,790,450);
  image(smiley,810,470,35,35);
  image(smiley,852,470,35,35);
  image(smiley,895,470,35,35);
  notification.play();
}
