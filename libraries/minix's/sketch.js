let theman;
let min_humans=7;
let human=[];

function preload(){
  theman=loadImage("theman.png");
}
function setup(){
  createCanvas(windowWidth,windowHeight);
}
function draw(){
  background(20,200,20);
  checkthenumber();
  showHuman();
  checkdistance();
}
function checkthenumber(){
  if(human.length<min_humans){
    human.push(new Human());
  }
}
function showHuman(){
  for(let i=0;i<human.length;i++){
  human[i].move();
  human[i].show();
  }
}
function checkdistance(){
  for(let i=0;i<human.length;i++)
  if(human[i].pos.y>windowHeight){
    human.splice(i,1);
  }
}
class Human {
  constructor(){ //initalize the objects
  this.speed=floor(random(4,7));
  this.size=floor(random(60,200));
  this.pos=new createVector(random(windowWidth),-5);
  }
move(){
  this.pos.y+=this.speed;
  }
show(){
  image(theman,this.pos.x,this.pos.y,this.size,this.size);
  }
}
