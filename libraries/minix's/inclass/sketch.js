let x = 0;
let y = 200;
let spacing=50;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(250,200,200);
  frameRate(100);
}

function draw() {
  stroke(255);
  if (random(1) > 1) {
    line(x, y, x+spacing, y+spacing);
  } else {
    line(x, y+spacing, x+spacing, y);
    stroke(0);
    if (random(1) > 0.5) {
      line(x, y, x+spacing, y+spacing);
    } else {
      line(x, y+spacing, x+spacing, y);
    }
}
  x+=10;
  if (x > width) {
    x = 0;
    y += spacing;
  }
  y+=10;
  if(y>600){
    y=0;
    x+=spacing;
  }
}
